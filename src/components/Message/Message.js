import React from 'react';
import './Message.css';

const Message = ({text, author, time}) => {
    return (
        <div className="message">
            <p><strong>Author: </strong>{author}</p>
            <p><strong>Message: </strong>{text}</p>
            <p><strong>message time: </strong>{time}</p>
        </div>
    );
};

export default Message;